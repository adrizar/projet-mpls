upgrade fpd auto
version 12.4
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname PE2-BACKUP
!
boot-start-marker
boot-end-marker
!
logging message-counter syslog
!
no aaa new-model
ip source-route
ip cef
!
!
!
!
ip vrf Ynov.1
 rd 1000:1
 route-target export 1000:1
 route-target import 1000:1
!
ip vrf Ynov.1b
 rd 1001:1
 route-target export 1001:1
 route-target import 1001:1
!
ip vrf Ynov.2b
 rd 2001:2
 route-target export 2001:2
 route-target import 2001:2
!
no ipv6 cef
!
multilink bundle-name authenticated
mpls ldp neighbor 5.5.5.5 password mplsito
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
memory-size iomem 0
archive
 log config
  hidekeys
!
!
!
!
!
!
!
!
!
interface Loopback1
 ip address 8.8.8.8 255.255.255.0
 ip ospf network point-to-point
!
interface FastEthernet0/0
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/0
 ip address 17.0.0.2 255.255.255.252
 duplex half
 mpls ip
!
interface Ethernet1/1
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/2
 ip vrf forwarding Ynov.2b
 ip address 20.0.0.2 255.255.255.252
 duplex half
!
interface Ethernet1/3
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/4
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/5
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/6
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/7
 no ip address
 shutdown
 duplex half
!
router ospf 1
 router-id 8.8.8.8
 log-adjacency-changes
 network 8.8.8.0 0.0.0.255 area 0
 network 17.0.0.0 0.0.0.3 area 0
 network 20.0.0.0 0.0.0.3 area 0
!
router bgp 2
 no synchronization
 bgp log-neighbor-changes
 neighbor 3.3.3.3 remote-as 1
 neighbor 3.3.3.3 update-source Loopback1
 neighbor 4.4.4.4 remote-as 2
 neighbor 4.4.4.4 update-source Loopback1
 no auto-summary
 !
 address-family vpnv4
  neighbor 3.3.3.3 activate
  neighbor 3.3.3.3 send-community both
  neighbor 4.4.4.4 activate
  neighbor 4.4.4.4 send-community both
 exit-address-family
 !
 address-family ipv4 vrf Ynov.2b
  no synchronization
 exit-address-family
 !
 address-family ipv4 vrf Ynov.1b
  no synchronization
 exit-address-family
 !
 address-family ipv4 vrf Ynov.1
  no synchronization
 exit-address-family
!
ip forward-protocol nd
no ip http server
no ip http secure-server
!
!
!
!
!
!
!
!
!
control-plane
!
!
!
mgcp fax t38 ecm
!
!
!
!
gatekeeper
 shutdown
!
!
line con 0
 exec-timeout 30000 0
 stopbits 1
line aux 0
 stopbits 1
line vty 0 4
 login
!
end
