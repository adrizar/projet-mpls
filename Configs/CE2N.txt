upgrade fpd auto
version 12.4
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname CE2-NOMINAL
!
boot-start-marker
boot-end-marker
!
vrf definition Ynov.2
!
logging message-counter syslog
!
no aaa new-model
ip source-route
ip cef
!
!
ip dhcp excluded-address 192.168.2.250 192.168.2.254
!
ip dhcp pool DATA
   network 192.168.2.0 255.255.255.0
   default-router 192.168.2.254
   dns-server 8.8.8.8
!
!
no ipv6 cef
!
multilink bundle-name authenticated
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
memory-size iomem 0
archive
 log config
  hidekeys
!
!
!
!
!
!
!
!
!
interface Loopback1
 ip address 9.9.9.9 255.255.255.0
!
interface FastEthernet0/0
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/0
 description VERS PE2 NOMINAL
 ip address 19.0.0.1 255.255.255.252
 duplex half
!
interface Ethernet1/1
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/2
 description VLAN DATA VRF Ynov.2
 ip address 192.168.2.253 255.255.255.0
 duplex half
 vrrp 1 description VERS SECOURS
 vrrp 1 ip 192.168.2.254
 vrrp 1 preempt delay minimum 5
 vrrp 1 priority 200
!
interface Ethernet1/3
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/4
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/5
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/6
 no ip address
 shutdown
 duplex half
!
interface Ethernet1/7
 no ip address
 shutdown
 duplex half
!
router eigrp 3
 network 9.9.9.0 0.0.0.255
 network 19.0.0.0 0.0.0.3
 network 192.168.2.0
 no auto-summary
!
ip forward-protocol nd
no ip http server
no ip http secure-server
!
!
!
!
!
!
!
!
!
control-plane
!
!
!
mgcp fax t38 ecm
!
!
!
!
gatekeeper
 shutdown
!
!
line con 0
 exec-timeout 30000 0
 stopbits 1
line aux 0
 stopbits 1
line vty 0 4
 exec-timeout 30000 0
 login
!
end
