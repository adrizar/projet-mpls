# Comptes rendus

## 05/04/23

### Fait

Configuration et test du Radius pour tous les routeurs sauf P, pas possible

### A faire

Terminer Radius
Débuter la partie Firewall 

### Fait

- Réorganisation de l'archi : supression vrf .1b pour avoir qu'une seule vrf et un seul AS BGP, config refaite au propre pour tous les routeurs
- Ajout PE, CE pour RADIUS et Firewall

### A faire

- Enlever la conf radius pour P en branchant le radius sur e1/5 car impossible via backbone MPLS et rajouter pour les autres PE CE switch, mettre ACL si besoin pour sécuriser
- Vérifier les sessions BGP des PE 
- Débuter la partie Firewall 

## 08/03/23

### Fait

- Config Radius et test sur le routeur adjacent P OK après difficultés
- Essayer de trouver un moyen pour lier le radius à l'archi MPLS avec des sous interfaces et encapsulation de vlan mais NOK à cause de l'image routeur

### A faire

- Elargir la config sur tous les routeurs avec du routage

## 22/02/23

### Fait

- Config pour que les CE BACKUP communiquent entre eux (routage des LAN et redistribution via PE)
- Débug puis ping quand CE BACKUP UP => OK
- Démarrage de la partie RADIUS pour l'authentification ssh centralisée TACACS des routeurs : AAA (Authentication, Authorization and Accounting)

### A faire

- Config Radius

## 18/01/23

Finalement, quand je voulais sauvegarder mes configs, je n'ai plus la main sur le web GUI !!! tout perdu.
**EDIT** : je sais pas comment mais j'ai réussi à me reconnecter au lab, donc configs rajoutées :)

### Fait

- Config vrf des 2 sites refaites pour le lien passant par le nominal d'un site vers l'autre, pas encore pour le backup
- Je pensais avoir terminé avec le routage EIGRP mais il me reste encore du travail pour la partie backup
- Test ping d'un PC vers l'autre OK !
- **Le projet étant long et complexe, je m'occuperais de la partie RADIUS, pare-feu et validation des tests le 2ème semestre, s'il me reste du temps en plus je pense faire du openvswitch**

## 04/01/23

### Fait

- Config Vrf Des 2 Sites 
- MP Bgp Mais Avec Difficultés Non Résolues Sur Le Routage Dynamique Eigrp Côté PE 

### A Faire

Résoudre Les Problèmes Eigrp
Et Terminer Les Configurations Puis Tester 

## 7/12/22

### Fait

- Recherche et configuration du Provider et du Provider Edge, OSPF, EIGRP pour LAN et MPLS des 2 côtés
- Vérification configuration vrf

### A faire prochaine fois

- Continuer la config vrf et routage dynamique LAN
- Faire la partie MP-BGP

## 23/11/22

### Fait

- Recherche et configuration du VRRP protocole de redondance de routeur sur CE1
- Configuration des switch, PC et routeurs CE1 nominal et backup (adressage IP, vrrp, DHCP, vrf, vlan)
- Conf Firewall remis à la fin du projet ou dans une 2ème partie avec Radius car projet trop long

### A faire prochaine fois

- Recherche et configuration du Provider et du Provider Edge au niveau BGP et VRRP
- Configuration du routage dynamique et distribution de route sur les CE

## 09/11/22

### Fait
- Installation des images Linux, Routeur Cisco IOS, IOL et firewall pfsense
- Création de la topologie réseau avec des routeurs nominal/backup en MPLS/VPN

### A faire prochaine fois

- Continuer la configuration des routeurs Customer Edge (CE) backup nominal
- Recherche sur la conf firewall en MPLS et autres fonctionnalités réseau

## 19/10/22

### Fait
- Installation d'Eve-ng sur VirtualBox, Filezilla, 
- Routeur et switch cisco avec licence mis en place sur Eve

### A faire prochaine fois

- Intégration d'image linux
- Commencer la topologie réseau sur eve
