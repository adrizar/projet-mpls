# [Projet MPLS]

## Description

Le but de ce projet est de mettre en oeuvre un lab réseau basé sur le réseau type opérateur MPLS/VPN et de jouer avec les technos LAN.

## Objectifs

- Améliorer mes compétences en réseau
- Monter une architecture réseau complexe

## Technologies et langages utilisés

- EVE-ng
- Cisco
- Routage dynamique : BGP OSPF EIGRP
- VLAN, DHCP, VRRP
Si le temps :
- Radius Authentification 802.1X
- Pare-feu pfsense pour une sortie internet en dehors du VPN

## Plan provisoire

- Séance 1 : Installer les appli, vm, images routeurs et commencer un schéma, se documenter
- Séance 2 : Commencer le réseau sous eve-ng
- Séance 3 : Suite
